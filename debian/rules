#! /usr/bin/make -f

export OMPI_MCA_plm_rsh_agent=/bin/false
export OMPI_MCA_rmaps_base_oversubscribe=1

%:
	dh $@ --max-parallel=2

override_dh_auto_configure:
	dh_auto_configure -- \
	-DDEAL_II_CXX_FLAGS="-Wno-nonnull-compare -Wno-address" \
	-DCMAKE_PREFIX_PATH="/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/openmpi;/usr/include/hdf5/openmpi" \
	-DCMAKE_BUILD_TYPE=DebugRelease \
	-DDEAL_II_ALLOW_AUTODETECTION=OFF \
	-DDEAL_II_ALLOW_BUNDLED=OFF \
	-DDEAL_II_ALLOW_PLATFORM_INTROSPECTION=OFF \
	-DDEAL_II_HAVE_FP_EXCEPTIONS=FALSE \
	-DDEAL_II_COMPONENT_DOCUMENTATION=ON \
	-DDEAL_II_WITH_ARPACK=ON \
	-DDEAL_II_WITH_ASSIMP=ON \
	-DDEAL_II_WITH_BOOST=ON \
	-DDEAL_II_WITH_BZIP2=ON \
	-DDEAL_II_WITH_GMSH=ON \
	-DDEAL_II_WITH_GSL=ON \
	-DDEAL_II_WITH_HDF5=ON \
	-DDEAL_II_WITH_LAPACK=ON \
	-DDEAL_II_WITH_MPI=ON \
	-DDEAL_II_WITH_MUPARSER=ON \
	-DDEAL_II_WITH_NETCDF=ON \
	-DDEAL_II_WITH_OPENCASCADE=ON \
	-DDEAL_II_WITH_P4EST=ON \
	-DDEAL_II_WITH_PETSC=ON \
	-DDEAL_II_WITH_SCALAPACK=ON \
	-DDEAL_II_WITH_SLEPC=ON \
	-DDEAL_II_WITH_SUNDIALS=ON \
	-DDEAL_II_WITH_THREADS=ON \
	-DDEAL_II_WITH_TRILINOS=ON \
	-DDEAL_II_WITH_UMFPACK=ON \
	-DDEAL_II_WITH_ZLIB=ON \
	-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=OFF \
	-DDEAL_II_BASE_NAME="deal.ii" \
	-DDEAL_II_DOCHTML_RELDIR=share/doc/libdeal.ii-doc/html \
	-DDEAL_II_DOCREADME_RELDIR=share/doc/libdeal.ii-doc \
	-DDEAL_II_EXAMPLES_RELDIR=share/doc/libdeal.ii-doc/examples \
	-DDEAL_II_LIBRARY_RELDIR=lib/$(DEB_HOST_MULTIARCH) \
	-DDEAL_II_PROJECT_CONFIG_RELDIR=share/cmake/deal.II \
	-DDEAL_II_SHARE_RELDIR=share/deal.ii/

override_dh_auto_test:
	dh_auto_test --max-parallel=1

TMP_DOC := debian/tmp/usr/share/doc/libdeal.ii-doc
override_dh_auto_install:
	dh_auto_install
	# Copy over all external html resources:
	cp -r doc/doxygen/deal.II/images $(TMP_DOC)/html/doxygen/deal.II/
	# Replace links to external html resources by local links:
	sed -i -e 's#"https://www.dealii.org/images/steps/developer/\(step[_-].*\)"#"images/\1"#g' \
		-e 's#"https://zenodo.org/badge/DOI/10.5281/#"images/#g' \
		$(TMP_DOC)/html/doxygen/deal.II/step_*.html
	sed -i -e 's#"http://www.dealii.org/images/#"images/#g' \
		$(TMP_DOC)/html/doxygen/deal.II/classFE_*.html
	sed -i -e 's#"https://upload.wikimedia.org/wikipedia/commons/#"images/#g' \
		$(TMP_DOC)/html/doxygen/deal.II/group_*.html
	# Remove external links to the W3C validator icon:
	find $(TMP_DOC)/html \
		-type f -name '*.html' -exec sed -i 's#\(<img.*w3.org.*\)</a>#</a>#' {} \;
	# Remove superfluous license file
	rm $(TMP_DOC)/LICENSE

override_dh_compress:
	# Do not compress example source code, and icons:
	dh_compress -X.cc -X.ico


VERSION := 9.0.1
get-orig-source:
	wget --no-clobber -O deal.ii-$(VERSION).tar.gz https://github.com/dealii/dealii/releases/download/v$(VERSION)/dealii-$(VERSION).tar.gz
	wget --no-clobber -O deal.ii-doc-$(VERSION).tar.gz https://github.com/dealii/dealii/releases/download/v$(VERSION)/dealii-$(VERSION)-offline_documentation.tar.gz
	mkdir deal.ii-$(VERSION)
	tar --strip-components=1 -C deal.ii-$(VERSION) -xzf deal.ii-$(VERSION).tar.gz
	mkdir -p deal.ii-$(VERSION)/doc/doxygen/deal.II
	tar -C deal.ii-$(VERSION) -xzf deal.ii-doc-$(VERSION).tar.gz doc/doxygen/deal.II/images
	set -e; cd deal.ii-$(VERSION); \
		rm -rf bundled
	tar -cJf deal.ii_$(VERSION).orig.tar.xz deal.ii-$(VERSION)
	rm -rf deal.ii-$(VERSION)
